"use strict"

import { Pokemon } from "./classPokemon";

// ICI LES INSTANCES POKEMON

let carapuce = new Pokemon('Carapuce', '../src/images/backpokemon/carapuce.gif', '../src/images/frontpokemon/carapuce.gif', 300);

let salameche = new Pokemon('Salameche', '../src/images/backpokemon/salameche.gif', '../src/images/frontpokemon/salameche.gif', 300);

let pikachu = new Pokemon('Pikachu', '../src/images/backpokemon/pikachu.gif', '../src/images/frontpokemon/pikachu.gif', 300);

let bulbizarre = new Pokemon('Bulbizarre', '../src/images/backpokemon/bulbizarre.gif', '../src/images/frontpokemon/bulbizarre.gif', 300);

let herbizarre = new Pokemon('Herbizarre', '../src/images/backpokemon/herbizarre.gif', '../src/images/frontpokemon/herbizarre.gif', 500);

let reptincel = new Pokemon('Reptincel', '../src/images/backpokemon/reptincel.gif', '../src/images/frontpokemon/reptincel.gif', 500);

let carabaffe = new Pokemon('Carabaffe', '../src/images/backpokemon/carabaffe.gif', '../src/images/frontpokemon/carabaffe.gif', 500);

let florizarre = new Pokemon('Florizarre', '../src/images/backpokemon/florizarre.gif', '../src/images/frontpokemon/florizarre.gif', 800);

let tortank = new Pokemon('Tortank', '../src/images/backpokemon/tortank.gif', '../src/images/frontpokemon/tortank.gif', 800);

let raichu = new Pokemon('Raichu', '../src/images/backpokemon/raichu.gif', '../src/images/frontpokemon/raichu.gif', 800);

let dracofeu = new Pokemon('Dracofeu', '../src/images/backpokemon/dracofeu.gif', '../src/images/frontpokemon/dracofeu.gif', 800);

let bibiche = new Pokemon('Bibiche','../src/images/backpokemon/bibiche.gif','../src/images/frontpokemon/bibiche.gif',800);

let mewtwo = new Pokemon('Mewtwo', '../src/images/backpokemon/mewtwo.gif', '../src/images/frontpokemon/mewtwo.gif', 1200);


let pokemonTab = [carapuce, salameche, pikachu, bulbizarre, reptincel, florizarre, tortank, raichu, herbizarre, mewtwo, carabaffe, dracofeu, bibiche];

let randomPokemonPlayer = pokemonTab[Math.floor(Math.random() * pokemonTab.length)];
let randomPokemonOpponent = pokemonTab[Math.floor(Math.random() * pokemonTab.length)];


document.querySelector("#imgPlayer").src = randomPokemonPlayer.avatarBack;
document.querySelector("#imgOpponent").src = randomPokemonOpponent.avatarFront;

document.querySelector("#lifePlayer").max = randomPokemonPlayer.life;
document.querySelector("#lifeOpponent").max = randomPokemonOpponent.life;

let opponent = randomPokemonOpponent;
let player = randomPokemonPlayer;



// ICI LES BOUTTONS

let btnAttack = document.querySelector('#btnAttack');
let btnHeal = document.querySelector('#btnHeal');
let btnRun = document.querySelector('#btnRun');
let button = document.querySelectorAll('.btn');
let pad = document.querySelector('#btn');
let btnon = document.querySelector('#btnon');
let ecran = document.querySelector('#ecran');
let ecranoff = document.querySelector('#ecranoff');
let random = document.querySelector('#btnrandom');
let btnsound = document.querySelector('#btnsound');
let imgOpponent = document.querySelector('#imgOpponent');
let imgPlayer = document.querySelector('#imgPlayer');



// ICI LES ZONES DE COMMENTAIRE

let commentairePlayer = document.querySelector('#commentairePlayer');
let commentaireOpponent = document.querySelector('#commentaireOpponent');

// ICI LES BARRES DE PROGRESSION

let lifeOpponent = document.querySelector('#lifeOpponent');
let lifePlayer = document.querySelector('#lifePlayer');

// ICI LES FONCTIONS ATTAQUE

function attackPlayer() { // l'attaque du player
  commentairePlayer.textContent = player.attack(opponent);


  imgPlayer.style.animationName = '';
  imgOpponent.style.animationName = 'opponent';


  lifeOpponent.value = opponent.life;
}
function attackOpponent() {
  commentaireOpponent.textContent = opponent.attack(player);

  imgOpponent.style.animationName = '';
  imgPlayer.style.animationName = 'player';

  setTimeout(() => {
    imgPlayer.style.animationName = '';
  }, 2000);

  lifePlayer.value = player.life;
  if (lifePlayer.value <= 0) {
    lifePlayer.value = 0;
    commentairePlayer.textContent = `${target.name} remporte le combat !`
  }
}
function randomOpponent() {
  if (lifeOpponent.value <= 100) {

    let tab = [healOpponent, attackOpponent];
    let choiceOpponent = tab[Math.floor(Math.random() * tab.length)];

    setTimeout(() => {
      choiceOpponent();
    }, 2000);

  } else {
    setTimeout(() => {
      attackOpponent();
    }, 2000);
  }
}

// ICI LES FONCTIONS HEAL

function healPlayer() { // on soigne le player
  commentairePlayer.textContent = player.heal();
  lifePlayer.value = player.life;
}

let music = new Audio('./music/music.ogg');

function healOpponent() { // on soigne le opponent
  commentaireOpponent.textContent = opponent.heal();
  lifeOpponent.value = opponent.life;
}


//  ICI LES FONCTIONS AUDIO

function audioPlay() {
  music.play();
}

function audioPause() {
  music.pause();
}

// ICI LES AUTRES FONCTIONS

function randomBattle() { // on fait un random sur les scenes de combat
  let battleTab = ['url(../src/images/bgbattle/battle1.png)', 'url(../src/images/bgbattle/battle2.png)', 'url(../src/images/bgbattle/battle3.png)', 'url(../src/images/bgbattle/battle4.png)', 'url(../src/images/bgbattle/battle5.png)'];
  let battleFight = battleTab[Math.floor(Math.random() * battleTab.length)];
  ecran.style.backgroundImage = battleFight;
}

function padDisplay() { // on cache le pad
  pad.style.display = 'none';

  setTimeout(() => {
    pad.style.display = '';
  }, 4000);
}

function emptyText() { // on vide les zones de texte
  setTimeout(() => {
    commentaireOpponent.innerHTML = '';
    commentairePlayer.innerHTML = '';
  }, 4000);
}

function run() {
  player.run();
  commentairePlayer.textContent = player.run();
  setTimeout(() => {
    window.location.reload();
  }, 2000);
}
// GAME

let gameOn = true; // pour remettre la page noire quand on clique sur on off

for (const btn of button) { // on boucle sur les bouttons 

  btn.addEventListener('click', () => {

    if (btn === btnAttack) { // code de l'attaque

      padDisplay();

      attackPlayer();

      randomOpponent();

      emptyText();

    } else if (btn === btnHeal) { // code de la potion

      padDisplay();

      randomOpponent();

      healPlayer();

      emptyText();

    } else if (btn === btnRun) { // code de la fuite
      run();

    } else if (btn === btnsound) {
      if (music.paused === true) {
        audioPlay();
      }
      else {
        audioPause();
      }

    } else if (btn === btnon) {

      if (gameOn) {
        ecranoff.style.display = 'none';
        gameOn = false;
        randomBattle();
      } else {
        ecranoff.style.display = 'initial';
        gameOn = true;
        window.location.reload();
      }

    } else if (btn === btnrandom) {
      randomBattle();
    }
  });
};

